import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class ConvertionService {

  constructor(private http: HttpClient) { }

  getExchangeRate(fromCurrency, toCurrency): Observable<any> {
    return this.http.get(`http://apilayer.net/api/live?access_key=80271153eaf2cbc94cd96277a58cf3e5&currencies=${toCurrency}&source=${fromCurrency}&format=1`)
      .pipe(
        map((resp: any) => {
          return resp.quotes[`${fromCurrency}${toCurrency}`];
        })
      );
  }

  getAvailableCurrencies(): Observable<any> {
    return this.http.get(`http://apilayer.net/api/live?access_key=80271153eaf2cbc94cd96277a58cf3e5`)
      .pipe(
        map((resp: any) => {
          const arrayConvertions = Object.keys(resp.quotes);
          return arrayConvertions.map(data => {
            if (data.startsWith('USD')) {
              return data.substring(3, data.length);
            }
          }
          );
        })
      );
  }

  getCountryNames(currencyCode): Observable<any> {
    return this.http.get(`https://restcountries.eu/rest/v2/currency/${currencyCode}`)
      .pipe(
        map((arraypaises: any[]) => arraypaises.map(pais => pais.name))
      );
  }





}
