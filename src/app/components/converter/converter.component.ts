import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ConvertionService } from '../../services/convertion.service';

@Component({
  selector: 'app-converter',
  templateUrl: './converter.component.html',
  styleUrls: ['./converter.component.sass']
})
export class ConverterComponent implements OnInit {
  public availableCountries = [];
  public message = '';
  formObject = {
    amountUsd: 20,
    currencyToCalculate: 'ARS'
  };
  constructor(private conversor: ConvertionService) { }

  ngOnInit(): void {
    this.conversor.getAvailableCurrencies().subscribe((array: any[]) => {
      this.availableCountries = array;
    });
  }

  sendForm(forma: NgForm): void {
    if (forma.valid) {
      this.conversor.getExchangeRate('USD', forma.value.currencyToCalculate)
        .subscribe(exchangeRate => {
          this.conversor.getCountryNames(forma.value.currencyToCalculate).subscribe(names => {
            this.message = `USD${forma.value.amountUsd} con un tipo de cambio de ${exchangeRate}, son equivalentes a ${forma.value.currencyToCalculate} ${(exchangeRate * forma.value.amountUsd)} y pueden ser utilizados en: ${ names }`;
          }
          );
        }
        );


    }

  }

}
